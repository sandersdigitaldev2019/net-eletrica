/**
 * Módulo Mega Menu
 * Montagem do Mega Menu
 */
document.addEventListener("touchstart", function () {
}, false);

$(function () {
    $('#wsnavtoggle').click(function () {
        $('.pusher').toggleClass('wsoffcanvasopener');
        $('body').addClass('active');
    });
    $('.overlapblackbg').click(function () {
        $('.pusher').toggleClass('wsoffcanvasopener');
        $('body').removeClass('active');
    });
    $('.wsmenu-list > li').has('.wsmenu-submenu').prepend('<span class="wsmenu-click"><i class="wsmenu-arrow icon angle right"></i></span>');
    $('.wsmenu-list > li').has('.wsshoptabing').prepend('<span class="wsmenu-click"><i class="wsmenu-arrow icon angle right"></i></span>');
    $('.wsmenu-list > li').has('.megamenu').prepend('<span class="wsmenu-click"><i class="wsmenu-arrow icon angle right"></i></span>');
    $('.wsmenu-click, .wsmenu-list > li > a').click(function () {
        $(this).siblings('.wsmenu-submenu').slideToggle();
        //$(this).siblings('.megamenu').slideToggle();
        $(this).siblings('.megamenu').addClass('active');
    });
    $('.megamenu .go_back').on('click', function (e) {
        e.preventDefault();
        $(this).parents('.megamenu').removeClass('active');
    });
    $('.wsmenu').swipeleft(function () {
        $('.pusher').toggleClass('wsoffcanvasopener');
    });
});